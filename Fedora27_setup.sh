#!/bin/bash

echo "##############################################"
echo "       		Updating"
echo "##############################################"
sudo dnf -y update 2>&1
sudo dnf -y upgrade 2>&1
echo "##############################################"
echo "       Installing some shitty utilities"
echo "##############################################"
sudo dnf -y install tilix zsh nano vim util-linux-user gnome-tweak-tool-3.26.4-1.fc27.noarch gnome-shell-extension-user-theme-3.26.2-1.fc27.noarch 2>&1
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s /bin/zsh
sed 's/ZSH_THEME="robbyrussell"/ZSH_THEME="cypher"/g' ~/.zshrc
git clone https://github.com/zsh-users/zsh-autosuggestions.git $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
source ~/.zshrc
echo "##############################################"
echo "          Pssst, now you've ZSH!!"
echo "##############################################"
echo
echo "##############################################"
echo "       	    Installing Fedy"
echo "##############################################"
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 2>&1
sudo dnf -y update 2>&1
sudo dnf -y upgrade 2>&1
sudo dnf -y install https://dl.folkswithhats.org/fedora/$(rpm -E %fedora)/RPMS/folkswithhats-release.noarch.rpm 2>&1
sudo dnf install -y fedy 2>&1
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim 2>&1
echo "##############################################"
echo "       	    Installing GOlang"
echo "##############################################"
wget -P ~/Downloads https://storage.googleapis.com/golang/go1.8.3.darwin-amd64.pkg 2>&1
sudo tar -C /usr/local -xzf Downloads/go1.8.3.linux-amd64.tar.gz 2>&1
export PATH=$PATH:/usr/local/go/bin
echo "##############################################"
echo "       	    Installing Rust"
echo "##############################################"
curl https://sh.rustup.rs -sSf | sh
echo "##############################################"
echo "       Installing some kickass(h) utils"
echo "##############################################"
go get -u github.com/moul/advanced-ssh-config/cmd/assh 2>&1
sudo mv ./go/bin/assh /usr/local/go/bin/ 
echo 'hosts:

  Rasp:
    Hostname: 192.168.0.12
    User: void
    Port: 602'  > assh.yml

assh config build > ~/.ssh/config 
echo "##############################################"
echo "     Putting some vintage Rust on your pc"
echo "##############################################"
curl https://sh.rustup.rs -sSf | sh
echo "##############################################"
echo "       	    Launching Fedy"
echo "##############################################"
fedy && echo 'remember to activate zsh plugins!'
